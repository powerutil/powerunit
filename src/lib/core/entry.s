/* Entry point. */
.text
    .p2align 4
    .global _start
_start:
    start
    run \testfile
.text  /* Reenter code section if test code exited. */
    end
.size _start, . - _start
