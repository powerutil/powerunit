/*
 * Define register names.
 * Defines [prefix]{[from]-[to]}
 * TODO: Is there a way to do this without the magic?
 *
 * Power asm uses plain numbers to refer to registers.
 * Defining prefixed register names increases code clarity.
 *
 * Repeat block iterates numbers being defined.
 * Inner IRP block provides single number as parameter.
 *
 * prefix - String. Register name prefix.
 * from - Integer. Lower bound of range to define.
 * to - Integer. Upper bound of range to define.
 */
.macro define_registers prefix:req from:req to:req
.Li = \from
.rept (\to - \from + 1)
    .irp num, %.Li
        .local \prefix\num
        .hidden \prefix\num
        .equiv \prefix\num, \num
    .endr
    .Li = .Li + 1
.endr
.endm

/* Define register names. */
define_registers  r, 0, 31  /* General. */
define_registers  f, 0, 31  /* Floating point. */
define_registers  v, 0, 31  /* Vector. */
define_registers vs, 0, 63  /* Vector scalar. */
define_registers cr, 0,  7  /* Condition field. */

/* Undefine register names macro. */
.purgem define_registers
