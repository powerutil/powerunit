/*
 * Run test.
 *
 * path - String. Required. Path to file containing test code.
 */
.macro run path:req
.ifnb \path
.include "\path"
.endif
.endm
