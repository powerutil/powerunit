/*
 * Set result message.
 * Clears result message pointer to null if no message provided.
 * Use in code section.
 *
 * Register usage
 *   r30  Addressing
 *   r31  Value stage
 *
 * str - Optional. String. Result message.
 */
.macro rmsg str
/* Stage null pointer if no message provided. */
.ifb \str
    li 31, 0
/* Assemble message and stage address. */
.else
LOCAL msgmem
.data
msgmem:
    .asciz \str
.previous
    lis 31, msgmem@ha
    la 31, msgmem@l(31)
.endif
/* Store message address to result data. */
    lis 30, result_message@ha
    std 31, result_message@l(30)
.endm
