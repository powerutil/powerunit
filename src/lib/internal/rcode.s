/*
 * Set result code.
 *
 * Register usage
 *   r30  Addressing
 *   r31  Value stage
 *
 * code - Required. Integer. Result code.
 */
.macro rcode code:req
    li 31, \code
    lis 30, result_code@ha
    stw 31, result_code@l(30)
.endm
