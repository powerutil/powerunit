/*
 * End run.
 * Writes result message to stderr.
 * Exits process with provided result code.
 * Use in code section.
 *
 * Register usage
 *   r0   Syscall number
 *   r3   Syscall arg
 *   r4   Syscall arg, result message address
 *   r5   Syscall arg, string length
 *   r30  Addressing
 *   r31  String byte stage
 *   cr0  Result message address test
 */
.macro end
LOCAL exit, count, endcount
/* Load result message address. */
    lis 4, result_message@ha
    ld 4, result_message@l(4)
/* Detect no result message. */
    cmplwi 4, 0
    beq exit
/* Count result message byte length. */
    li 5, 0
count:
    lbzx 31, 5, 4
    cmplwi 31, 0
    beq endcount
    addi 5, 5, 1
    b count
endcount:
/* Write result message to stderr. */
    li 3, 2  /* stream: stderr */
    li 0, 4  /* syscall: write */
    sc
/* Write line break to stderr. */
    li 3, 2  /* stream: stderr */
    lis 4, linebreak@ha
    la 4, linebreak@l(4)
    li 5, 1
    li 0, 4  /* syscall: write */
    sc
exit:
/* Stage result code as exit code. */
    lis 30, result_code@ha
    lwz 3, result_code@l(30)
/* Exit process. */
    li 0, 1  /* syscall: exit */
    sc
.endm
