/*
 * Start run.
 * Output header.
 *
 * Register usage
 *   r0   Syscall number
 *   r3   File descriptor
 *   r4   String address
 *   r5   String byte length
 */
.macro start
/* Load header address. */
    lis 4, header@ha
    la 4, header@l(4)
/* Stage header byte length. */
    li 5, 19
/* Write header to stdout. */
    li 3, 1  /* stream: stdout */
    li 0, 4  /* syscall: write */
    sc
/* Write line break to stderr. */
    li 3, 1  /* stream: stdout */
    lis 4, linebreak@ha
    la 4, linebreak@l(4)
    li 5, 1
    li 0, 4  /* syscall: write */
    sc
.endm
