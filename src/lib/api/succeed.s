/*
 * Succeed test.
 * Use in code section.
 *
 * msg - Optional. String. Result message.
 */
.macro succeed msg=""
    rmsg \msg
    rcode 0
    end
.endm
