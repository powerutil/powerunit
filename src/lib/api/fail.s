/*
 * Fail test.
 * Use in code section.
 *
 * msg - Required. String. Failure message.
 */
.macro fail msg:req
    rmsg \msg
    rcode 1
    end
.endm
