VPATH = src/lib

internal = internal/start.s internal/end.s internal/run.s internal/rcode.s \
	internal/rmsg.s
api = api/succeed.s api/fail.s
source = core/ifaces.s \
	core/usage.s core/mode.s core/env.s core/register.s \
	${internal} core/validate.s ${api} \
	core/static.s core/variable.s core/entry.s \
	core/ifacee.s

powerunit.s : ${source}
	mkdir -p build \
	&& cd src/lib \
	&& ../../util/combine ${source} \
	> ../../build/powerunit.s
